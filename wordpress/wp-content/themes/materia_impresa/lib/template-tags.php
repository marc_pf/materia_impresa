<?php
namespace Matimp\TemplateTags;

function subcategory_filter($category = NULL) {
	$category || $category = get_queried_object();

	if ( !is_a($category, 'WP_Term') ) return;

	$subcategories = get_terms(array(
		'taxonomy' => $category->taxonomy,
		'parent' => $category->term_id,
		'hide_empty' => false
	));

	if ( count($subcategories) ) : ?>
		<ul class='post-filter post-filter--tax'>
			<?php foreach ($subcategories as $subcategory) :
				$term_class = "{$subcategory->taxonomy}-{$subcategory->slug}";
				?>
				<li class='post-filter__term'><a class='js-filter-posts' href='<?= get_term_link($subcategory) ?>' data-filter-class='<?= $term_class ?>'><?= $subcategory->name ?></a></li>
			<?php endforeach; ?>
		</ul>
	<?php endif;
}
