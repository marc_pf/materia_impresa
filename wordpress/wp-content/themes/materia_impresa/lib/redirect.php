<?php

namespace Matimp\Redirect;

use Matimp\Utils;

function template_redirect() {
	if ( ! is_singular()
		|| !($post_id = get_queried_object_ID())
		|| ! Utils\page_is_empty($post_id)
		|| !($first_child = Utils\get_first_child($post_id))
		) return;

	$url = get_the_permalink($first_child->ID);

	wp_redirect($url, 303);
	exit;
}
add_action('template_redirect', __NAMESPACE__ . '\\template_redirect');
