<?php

namespace Matimp\Utils;

function get_first_child($post = 0) {
	$post = get_post($post);
	$children = get_children(array(
		'post_parent' => $post->ID,
		'post_type'   => $post->post_type,
		'numberposts' => 1,
		'orderby'     => 'menu_order',
		'order'       => 'ASC',
		'post_status' => 'publish'
	));

	foreach ($children as $child) {
		return $child;
	}

	return false;
}

function get_final_content($post_id = 0) {
	$content = get_the_content($post_id);
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	return $content;
}

function page_is_empty($post_id = 0) {
	$content = get_final_content($post_id);
	return empty($content);
}

function category_index($cat) {
	static $cat_table;

	if (empty($cat_table)) {
		global $sitepress;
		$sitepress && remove_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1 );

		$cat_table = array();
		$pending = array();
		$terms = get_terms(array(
			'taxonomy' => 'category',
			'hide_empty' => false
		));

		$i = 1;

		if ($terms) foreach ($terms as $term) {
			$term_id = default_lang_object_id($term->term_id, 'category');
			if ($term->term_id !== $term_id) $term = get_term( $term_id, 'category' );
			$parent = $term;
			while ($parent->parent) {
				$parent = get_term($parent->parent);
			}
			$parent_id = $parent->term_id;

			if ($term_id !== $parent_id) {
				if ( isset($cat_table[$parent_id]) ) {
					$cat_table[$term_id] = $cat_table[$parent_id];
				}
				else {
					isset($pending[$parent_id]) || $pending[$parent_id] = array();
					$pending[$parent_id][] = $term_id;
				}
			}
			else {
				$cat_table[$term_id] = $i;

				if (isset($pending[$term_id])) foreach ($pending[$term_id] as $child_id) {
					$cat_table[$child_id] = $i;
				}

				$i++;
			}
		}

		$sitepress && add_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1, 1 );
	}

	$cat = default_lang_object_id($cat, 'category');

	return $cat_table[$cat];
}

function default_lang_object_id($id, $type, $original_if_missing = true) {
	static $default_lang;

	$default_lang || $default_lang = apply_filters('wpml_default_language', NULL);

	return apply_filters( 'wpml_object_id', $id, $type, $original_if_missing, $default_lang );
}

function get_field($field_slug, $id, $do_formatting = false) {
	if ( function_exists('\get_field') ) {
		return \get_field($field_slug, $id, $do_formatting);
	}

	return false;
}
