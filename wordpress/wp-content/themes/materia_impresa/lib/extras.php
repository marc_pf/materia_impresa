<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;
use Matimp\Utils;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

function post_class($classes, $class, $post_id) {
    $terms = get_the_terms($post_id, 'category');
    $multiple = false;
    $registered_term_id;

    if ($terms) foreach ($terms as $term) {
        $current_term_id = $term->term_id;

        if ( !isset($registered_term_id) || term_is_ancestor_of($registered_term_id, $current_term_id, 'category') ) {
            $registered_term_id = $current_term_id;
        }
        elseif ( ! term_is_ancestor_of($current_term_id, $registered_term_id, 'category') ) {
            $multiple = true;
            break;
        }
    }

    $cat_index = $multiple ? 'mult' : isset($registered_term_id) ? Utils\category_index($registered_term_id) : 0;
    $classes[] = "cat-{$cat_index}";

  return $classes;
}
add_filter('post_class', __NAMESPACE__ . '\\post_class', 10, 3);

function category_colors() {
    $terms = get_terms(array(
        'taxonomy' => 'category',
        'hide_empty' => false
    ));

    if ($terms) : ?>
        <style>
            <?php foreach($terms as $term) :
                $term_id = Utils\default_lang_object_id($term->term_id, 'category');
                $id = "category_{$term_id}";
                $color = Utils\get_field('cat-color', $id);
                if ($color) : ?>
                    .category-<?= $term->slug ?> .article__image {
                        border-top-color: <?= $color ?>;
                    }
                <?php endif;
            endforeach; ?>
        </style>
    <?php endif;
}
add_action('wp_head', __NAMESPACE__ . '\\category_colors');
