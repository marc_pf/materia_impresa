/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
				var $body = $(document.body);
				var $menu = $('.js-menu');
				var $menu_button = $('.js-show-menu');
				var menu_clicked = false;
				var menu_visible = false;

				$menu.click(function(){
					console.log('Menu clicked!');
					menu_clicked = true;
				});

        $menu_button.click(function(){
            $menu_button.toggleClass('is-active');
            $menu.toggleClass('visible');
						$body.toggleClass('menu-visible');
						menu_visible = !menu_visible;

						if (menu_visible) {
							console.log('Menu visible!');

							$body.on('click.matimp_close_menu', function(){
								console.log('Body Clicked!');
								if (menu_clicked) {
									menu_clicked = false;
								}
								else {
									$menu_button.removeClass('is-active');
			            $menu.removeClass('visible');
									$body.removeClass('menu-visible');
									menu_visible = false;

									$body.off('click.matimp_close_menu');
								}
							});

							return false;
						}
						else {
							$body.off('click.matimp_close_menu');
						}
        });


				var $previous_posts = $('.nav-previous');
				if ($previous_posts.length) {
					var $infinite_scroll = $('.js-infinite-scroll');

	        $('.infinite-scroll-status').show();

	        $('.js-infinite-scroll').infiniteScroll({
	            path: '.nav-previous a',
	            append: '.article',
	            prefill: true,
	            hideNav: '.nav-previous',
	            history: false,
	            status: '.infinite-scroll-status'
	        });
				}

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
