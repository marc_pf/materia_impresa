<article <?php post_class('thumb col-6 col-md-3 col-lg-2'); ?>>
    <figure class='thumb__image'>
        <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail() ?>
        </a>
    </figure>
    <h3 class="thumb__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
</article>
