<?php

if (is_home()) : ?>
	<nav class="sub-header menu home-menu">
		<?php wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav menu__nav')); ?>
	</nav>
<?php endif;
