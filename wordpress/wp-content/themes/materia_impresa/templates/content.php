<?php
/**
 * Loop Name: Full Content
 *
 */
the_content();
wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>'));
