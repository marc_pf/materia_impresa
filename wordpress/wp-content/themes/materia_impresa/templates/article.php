<?php
$overlay_class = 'article__overlay--box-' . rand(1, 5);
$all_terms = get_the_terms($post, 'category');
$terms = '';

$current_term = 0;

if ( is_category() ) {
	$current_term = get_queried_object_ID();
}

foreach ($all_terms as $term) {
	if ( !$current_term || ( $term->term_id != $current_term && !term_is_ancestor_of($term, $current_term, 'category') ) ) {
		$link = get_category_link($term);
		if ($terms) $terms .= ', ';
		$terms .= "<a href='{$link}'>{$term->name}</a>";
	}
}
?>
<article <?php post_class('article col-md-6 col-lg-4'); ?>>
	<div class='article__inner'>
		<figure class='article__image'>
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail() ?>
			</a>
			<div class='article__overlay <?= $overlay_class ?>'>
				<div class='article__overlay-content'>
					<!-- <div class="entry-summary article__excerpt">
							<?php //the_excerpt(); ?>
					</div> -->
					<div class="article__cats">
							<?php echo $terms; ?>
					</div>
					<div class="article__more">
						<a href="<?php the_permalink(); ?>">+</a>
					</div>
				</div>
			</div>
		</figure>
		<header class='article__header'>
			<h2 class="article__title">
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</h2>
		</header>
	</div>
</article>
