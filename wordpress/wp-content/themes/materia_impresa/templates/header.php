<?php
use Roots\Sage\Assets;
?>

<header class="banner">
    <div class="container banner__container">
        <div class='row banner__row'>
            <div class='banner__main banner__item'>
                <a class="brand banner__brand" href="<?= esc_url(home_url('/')); ?>">
                    <img class='brand__logo logo' src="<?= Assets\asset_path('images/logo.svg'); ?>" alt="Matèria Impresa" />
                    <!-- <h1 class='brand__title'>Matèria Impresa</h1> -->
                </a>
            </div>
            <div class="banner__menus banner__item">
              <nav class="menu banner__menu menu--top ">
                  <?php
                  if (has_nav_menu('top_navigation')) :
                      wp_nav_menu(array('theme_location' => 'top_navigation', 'menu_class' => 'nav menu__nav'));
                  endif;
                  ?>
              </nav>
              <nav class="menu banner__menu menu--lang">
                  <?php //TODO: put lang selector here ?>
                  <?php
                  if (has_nav_menu('lang_navigation')) :
                      wp_nav_menu(array('theme_location' => 'lang_navigation', 'menu_class' => 'nav menu__nav'));
                  endif;
                  ?>
              </nav>
            </div>
            <div class='menu-button banner__menu-button banner__item'>
                <button class="hamburger hamburger--collapse js-show-menu" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</header>
