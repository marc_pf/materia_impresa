<?php
/**
 * Loop Name: Post Grid
 *
 * Post loop for use with the SiteOrigin Post Loop widget in Page Builder.
 *
 *
 */
?>

<div class='row js-infinite-scroll'>
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/article', $post->post_type); ?>
	<?php endwhile; ?>
</div>
