<?php
/**
 * Loop Name: Thumbs
 *
 * Post loop for use with the SiteOrigin Post Loop widget in Page Builder.
 *
 *
 */
?>

<div class='row'>
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/article', 'thumb'); ?>
	<?php endwhile; ?>
</div>
